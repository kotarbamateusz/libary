<?php

use yii\db\Migration;

/**
 * Class m180620_180657_add_permissions
 */
class m180620_180657_add_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('permissions', ['permission' => 'admin']);
        $this->insert('permissions', ['permission' => 'user']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180620_180657_add_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180620_180657_add_permissions cannot be reverted.\n";

        return false;
    }
    */
}
